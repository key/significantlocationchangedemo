//
//  AppDelegate.h
//  SignificantLocationChangeDemo
//
//  Created by Mitsukuni Sato on 3/8/13.
//  Copyright (c) 2013 MyBike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  ViewController.m
//  SignificantLocationChangeDemo
//
//  Created by Mitsukuni Sato on 3/8/13.
//  Copyright (c) 2013 MyBike. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "ViewController.h"

#define kPolylineSignificant @"significant"
#define kPolylineUserLocation @"userLocation"


@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIButton *startStopButton;
@property (strong, nonatomic) IBOutlet UISwitch *userTrackSwitch;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic) BOOL isMonitoring;

@property (strong, nonatomic) NSMutableArray *significantLocations;
@property (strong, nonatomic) NSMutableArray *userLocations;
@property (strong, nonatomic) MKPolyline *significantPolyline;
@property (strong, nonatomic) MKPolyline *userLocationPolyline;

@end

@implementation ViewController

#pragma mark - lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.startStopButton setTitle:@"Start significant location update monitoring" forState:UIControlStateNormal];
    [self.startStopButton setTitle:@"Stop location update" forState:UIControlStateSelected];

    if ([CLLocationManager locationServicesEnabled] && [CLLocationManager significantLocationChangeMonitoringAvailable]) {
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        self.isMonitoring = [[ud objectForKey:@"is_monitoring"] boolValue];
        if (!self.isMonitoring) {
            self.isMonitoring = FALSE;
        }

        [self setLocationManager:[[CLLocationManager alloc] init]];
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
        [self.locationManager setDelegate:self];
        [self.userTrackSwitch setOn:YES];
        [self.startStopButton setSelected:self.isMonitoring];
        [self setSignificantLocations:[[NSMutableArray alloc] initWithCapacity:0]];
        [self setUserLocations:[[NSMutableArray alloc] initWithCapacity:0]];

        UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
        [panGesture setDelegate:self];
        [self.mapView setDelegate:self];
        [self.mapView addGestureRecognizer:panGesture];
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];

        [self setSignificantPolyline:[MKPolyline polylineWithCoordinates:nil count:0]];
        [self setUserLocationPolyline:[MKPolyline polylineWithCoordinates:nil count:0]];
        [self.mapView addOverlay:self.significantPolyline];
        [self.mapView addOverlay:self.userLocationPolyline];

        [self startStopUpdateLocation];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.mapView setShowsUserLocation:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.mapView setShowsUserLocation:FALSE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Actions
- (IBAction)startTapped:(id)sender
{
    self.isMonitoring = !self.isMonitoring;

    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:self.isMonitoring forKey:@"is_monitoring"];
    [ud synchronize];

    [self.startStopButton setSelected:self.isMonitoring];
    [self startStopUpdateLocation];
}

- (void)startStopUpdateLocation
{
    if (self.isMonitoring) {
        NSLog(@"START: monitoring significant location change");
        [self.locationManager startMonitoringSignificantLocationChanges];
    } else {
        NSLog(@"STOP: monitoring significant location change");
        [self.locationManager stopMonitoringSignificantLocationChanges];
    }
}

- (IBAction)switchTapped:(id)sender
{
    [self.mapView setUserTrackingMode:MKUserTrackingModeNone];
}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    NSLog(@"User location updated: location=%@", [userLocation description]);

    if (userLocation != nil) {
        CLLocationCoordinate2D coordinate = userLocation.coordinate;

        if ([self coordinate2DIsValid:coordinate]) {
            NSValue *value = [NSValue valueWithMKCoordinate:coordinate];
            [self addLocation:self.userLocations value:value];
            [self drawPolyline:self.userLocations polyline:self.userLocationPolyline];
        }
    }
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineView *view = [[MKPolylineView alloc] initWithOverlay:overlay];

    UIColor *color;
    if ([(MKPolyline *)overlay.title isEqual:kPolylineSignificant]) {
        color = [UIColor blueColor];
    } else if ([(MKPolyline *) overlay.title isEqual:kPolylineUserLocation]) {
        color = [UIColor redColor];
    }

    [view setStrokeColor:color];
    [view setLineWidth:5.0f];
    [view setAlpha:0.5];

    return view;
}


#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"Location updated: locations=%@", locations);
    CLLocation *location = (CLLocation *) [locations objectAtIndex:0];
    if (location != nil) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);

        if ([self coordinate2DIsValid:coordinate]) {
            [self addLocation:self.significantLocations value:[NSValue valueWithMKCoordinate:coordinate]];
            [self drawPolyline:self.significantLocations polyline:self.significantPolyline];

            // notification
            UILocalNotification *localNotif = [[UILocalNotification alloc] init];
            if (localNotif == nil) {
                return;
            }

            localNotif.fireDate = [NSDate date];
            localNotif.timeZone = [NSTimeZone defaultTimeZone];
            localNotif.alertBody = [NSString stringWithFormat:@"通知を受信しました。"];
            localNotif.alertAction = @"Open";
            localNotif.soundName = UILocalNotificationDefaultSoundName;
            localNotif.applicationIconBadgeNumber = 1;
            NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"通知を受信しました。" forKey:@"EventKey"];
            localNotif.userInfo = infoDict;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];

            [self.mapView setCenterCoordinate:self.mapView.userLocation.coordinate];
        }
    }
}


#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


#pragma mark - private methods
- (void)addLocation:(NSMutableArray *)array value:(NSValue *)value
{
    CLLocationCoordinate2D coordinate;
    [value getValue:&coordinate];
    if (CLLocationCoordinate2DIsValid(coordinate)) {
        [array addObject:value];
    }
}

- (void)drawPolyline:(NSArray *)array polyline:(MKPolyline *)polyline
{
    [self.mapView removeOverlay:polyline];

    CLLocationCoordinate2D lineCoordinates[[array count] + 1];

    for (int i=0; i < [array count]; i++) {
        CLLocationCoordinate2D value;
        [[array objectAtIndex:i] getValue:&value];
        lineCoordinates[i] = value;
    }

    if ([polyline isEqual:self.significantPolyline]) {
        [self setSignificantPolyline:[MKPolyline polylineWithCoordinates:lineCoordinates count:[self.significantLocations count]]];
        [self.significantPolyline setTitle:kPolylineSignificant];
    } else if ([polyline isEqual:self.userLocationPolyline]) {
        [self setUserLocationPolyline:[MKPolyline polylineWithCoordinates:lineCoordinates count:[self.userLocations count]]];
        [self.userLocationPolyline setTitle:kPolylineUserLocation];
    }
    [self.mapView addOverlay:polyline];
}

- (void)handlePanGesture:(UIPanGestureRecognizer *)sender
{
    [self.mapView setUserTrackingMode:MKUserTrackingModeNone];
    [self.userTrackSwitch setOn:FALSE animated:YES];
}


- (BOOL)coordinate2DIsValid:(CLLocationCoordinate2D)coordinate
{
    CLLocationDegrees latitude = coordinate.latitude;
    CLLocationDegrees longitude = coordinate.longitude;

    if (latitude != 0 && longitude != 0 && CLLocationCoordinate2DIsValid(coordinate)) {
        return TRUE;
    }
    return FALSE;
}

@end

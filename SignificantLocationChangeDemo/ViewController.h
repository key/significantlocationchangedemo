//
//  ViewController.h
//  SignificantLocationChangeDemo
//
//  Created by Mitsukuni Sato on 3/8/13.
//  Copyright (c) 2013 MyBike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate>

@end

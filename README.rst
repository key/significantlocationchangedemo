SignificantLocationChangeDemo
=============================

This project is SignificantLocationChange demo application for iPhone with iOS 6 or later.
Plot red line of getting user location, plot blue line of getting significant location.

.. image:: https://bitbucket.org/key/significantlocationchangedemo/raw/master/SignificantLocation.png
